# Description
The underlying PeerTube service already provides a web browser based peering mechanism for scalability of highly demanded content. In addition, we will deploy a distributed caching network for streaming and text/image based content.


*Content Delivery* and its *Quality-Of-Service* is a core service of this project. To serve the amount of target audience we anticipate, data throuphput and geo-localized/-optimized delivery become crucial issues.

*PeerTube*, the service used to host and deliver video content, already provides mechanisms that transparently enable data sharing between peers. This partly offloads the strain on the server infrastructure to the clients. The project follows this principle further and aims to create a participatory and distributed *Content Delivery Network* for all of the multimodal content distributed by it.

