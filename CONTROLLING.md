# Workpackage/Task

## DISPL.EU 2023

* *Name:* **Content Delivery Network (CDN)**
* *Task-ID:* **T2.6**
* *Participants:*
  * **fairkom**
* *ERP:* **[TASK00157](https://erp.fairkom.net/app/task/TASK00157)**
